
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data User
      </h1>
    </section>
 
    <!-- Main content -->
    <section class="content">
      <div>
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id='FrmAjax' class="form-horizontal" method="post">
              <div class="box-body">
                  <div class="form-group">
                    <label for="input_username" class="col-sm-2 control-label">Username</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name='username' id="username" placeholder="Username">
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="input_password" class="col-sm-2 control-label">Password</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="password" id="password" placeholder="Password">
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="level" class="col-sm-2 control-label">Level</label>

                      <div class="col-sm-10">
                        <select name="level" class="form-control" >
                          <option>level1</option>
                          <option>level2</option>
                        </select>
                      </div>
                  </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-primary" href="<?=base_url()?>index.php/user" role="button">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script src="<?php echo base_url();?>assets/jquery/jquery.min.js" type="text/javascript"></script>

<script>
    // action save
    $("#FrmAjax").on('submit',(function(e){
      //confirm ("sss")
      e.preventDefault();
      //alert ("abc")
      $.ajax({
        url : "<?php echo base_url()?>index.php/user/save",
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
        {
          if (data == '1'){
                alert('data  tersimpan')
                location.assign("<?=base_url()?>index.php/user")
          }else{
                alert('data gagal tersimpan')
            }
        }
      });
      }));
</script>