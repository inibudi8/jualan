
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Buku
      </h1>
    </section>
 
    <!-- Main content -->
    <section class="content">
      <div>
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Buku</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id='FrmAjax' class="form-horizontal" method="post">
              <div class="box-body">
                  <div class="form-group">
                    <label for="input_nama_buku" class="col-sm-2 control-label">Nama Buku</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name='nm_buku' id="nm_buku" placeholder="Nama Buku">
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="input_pengarang" class="col-sm-2 control-label">Pengarang</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="pengarang" id="pengarang" placeholder="Pengarang">
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="input_tahun_terbit" class="col-sm-2 control-label">Tahun Terbit</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="tahun_terbit" id="tahun_terbit" placeholder="Tahun Terbit">
                      </div>
                  </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-primary" href="<?=base_url()?>index.php/buku" role="button">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script src="<?php echo base_url();?>assets/jquery/jquery.min.js" type="text/javascript"></script>

<script>
    // action save
    $("#FrmAjax").on('submit',(function(e){
      //confirm ("sss")
      e.preventDefault();
      //alert ("abc")
      $.ajax({
        url : "<?php echo base_url()?>index.php/buku/save",
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
        {
          if (data == '1'){
                alert('data  tersimpan')
                location.assign("<?=base_url()?>index.php/buku")
          }else{
                alert('data gagal tersimpan')
            }
        }
      });
      }));
</script>