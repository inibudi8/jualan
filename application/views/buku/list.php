
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Buku
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
            <a href="<?php base_url ()?>buku/form"> <button type="button" class="btn btn-block btn-primary">ADD</button></a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID buku</th>
                  <th>Nama buku</th>
                  <th>Pengarang</th>
                  <th>Tahun Terbit</th>
                  <th>Edit</th>
			
                </tr>
                </thead>
                <tbody>
                <!--ini untuk isi data -->
                <?php 
                    foreach($buku->result_array() as $row) {
                ?>
                  <tr>
                    <td><?=$row['kode_buku']?></td>
                    <td><?=$row['nm_buku']?></td>
                    <td><?=$row['pengarang']?></td>
                    <td><?=$row['tahun_terbit']?></td>
                    <td>
                      <a href="<?=base_url()?>index.php/buku/form_edit/<?=$row['kode_buku']?>"><i class="fa fa-edit"> </i> Edit</a>>
                    </td>
					
                  </tr>
					<?php }?> 
					
                <!-- end isi data -->
                
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->